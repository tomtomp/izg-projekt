/******************************************************************************
 * Projekt - Zaklady pocitacove grafiky - IZG
 * spanel@fit.vutbr.cz
 *
 * $Id:$
 */

#include "student.h"
#include "transform.h"
#include "fragment.h"

#include <memory.h>
#include <math.h>

typedef S_Coords *ptr_S_Coords;

/*****************************************************************************
 * Globalni promenne a konstanty
 */

/* Typ/ID rendereru (nemenit) */
const int           STUDENT_RENDERER = 1;

const S_Material    MAT_WHITE_AMBIENT  = { 1.0, 1.0, 1.0, 1.0 };
const S_Material    MAT_WHITE_DIFFUSE  = { 1.0, 1.0, 1.0, 1.0 };
const S_Material    MAT_WHITE_SPECULAR = { 1.0, 1.0, 1.0, 1.0 };
const float FRAME_LENGTH = 100.0f;
int numTicks = 0;


/*****************************************************************************
 * Funkce vytvori vas renderer a nainicializuje jej
 */

S_Renderer * studrenCreate()
{
    S_StudentRenderer * renderer = (S_StudentRenderer *)malloc(sizeof(S_StudentRenderer));
    IZG_CHECK(renderer, "Cannot allocate enough memory");

    /* inicializace default rendereru */
    renderer->base.type = STUDENT_RENDERER;
    renInit(&renderer->base);

    /* nastaveni ukazatelu na upravene funkce */
    /* napr. renderer->base.releaseFunc = studrenRelease; */
    /* ??? */
    renderer->base.releaseFunc = studrenRelease;
    renderer->base.projectTriangleFunc = studrenProjectTriangle;

    /* inicializace nove pridanych casti */
    /* ??? */

    renderer->texture_data = loadBitmap(TEXTURE_FILENAME, &(renderer->texture_width), &(renderer->texture_height));

    return (S_Renderer *)renderer;
}

/*****************************************************************************
 * Funkce korektne zrusi renderer a uvolni pamet
 */

void studrenRelease(S_Renderer **ppRenderer)
{
    S_StudentRenderer * renderer;

    if( ppRenderer && *ppRenderer )
    {
        /* ukazatel na studentsky renderer */
        renderer = (S_StudentRenderer *)(*ppRenderer);

        /* pripadne uvolneni pameti */
        /* ??? */
        free(renderer->texture_data);
        
        /* fce default rendereru */
        renRelease(ppRenderer);
    }
}

/******************************************************************************
 * Nova fce pro rasterizaci trojuhelniku s podporou texturovani
 * Upravte tak, aby se trojuhelnik kreslil s texturami
 * (doplnte i potrebne parametry funkce - texturovaci souradnice, ...)
 * v1, v2, v3 - ukazatele na vrcholy trojuhelniku ve 3D pred projekci
 * n1, n2, n3 - ukazatele na normaly ve vrcholech ve 3D pred projekci
 * x1, y1, ... - vrcholy trojuhelniku po projekci do roviny obrazovky
 */

void studrenDrawTriangle(S_Renderer *pRenderer,
                         S_Coords *v1, S_Coords *v2, S_Coords *v3,
                         S_Coords *n1, S_Coords *n2, S_Coords *n3,
                         int x1, int y1, double h1,
                         int x2, int y2, double h2,
                         int x3, int y3, double h3,
                         S_Coords *t1, S_Coords *t2, S_Coords *t3
                         )
{
    int         minx, miny, maxx, maxy;
    int         a1, a2, a3, b1, b2, b3, c1, c2, c3;
    int         s1, s2, s3;
    int         x, y, e1, e2, e3;
    double      alpha, beta, gamma, w1, w2, w3, z, u, v, denom;
    S_RGBA      col1, col2, col3, color;

    const double oh1 = 1.0 / h1;
    const double oh2 = 1.0 / h2;
    const double oh3 = 1.0 / h3;
    const double ux1 = t1->x * oh1;
    const double ux2 = t2->x * oh2;
    const double ux3 = t3->x * oh3;
    const double uy1 = t1->y * oh1;
    const double uy2 = t2->y * oh2;
    const double uy3 = t3->y * oh3;

    IZG_ASSERT(pRenderer && v1 && v2 && v3 && n1 && n2 && n3);

    /* vypocet barev ve vrcholech */
    col1 = pRenderer->calcReflectanceFunc(pRenderer, v1, n1);
    col2 = pRenderer->calcReflectanceFunc(pRenderer, v2, n2);
    col3 = pRenderer->calcReflectanceFunc(pRenderer, v3, n3);

    /* obalka trojuhleniku */
    minx = MIN(x1, MIN(x2, x3));
    maxx = MAX(x1, MAX(x2, x3));
    miny = MIN(y1, MIN(y2, y3));
    maxy = MAX(y1, MAX(y2, y3));

    /* oriznuti podle rozmeru okna */
    miny = MAX(miny, 0);
    maxy = MIN(maxy, pRenderer->frame_h - 1);
    minx = MAX(minx, 0);
    maxx = MIN(maxx, pRenderer->frame_w - 1);

    /* Pineduv alg. rasterizace troj.
       hranova fce je obecna rovnice primky Ax + By + C = 0
       primku prochazejici body (x1, y1) a (x2, y2) urcime jako
       (y1 - y2)x + (x2 - x1)y + x1y2 - x2y1 = 0 */

    /* normala primek - vektor kolmy k vektoru mezi dvema vrcholy, tedy (-dy, dx) */
    a1 = y1 - y2;
    a2 = y2 - y3;
    a3 = y3 - y1;
    b1 = x2 - x1;
    b2 = x3 - x2;
    b3 = x1 - x3;

    /* koeficient C */
    c1 = x1 * y2 - x2 * y1;
    c2 = x2 * y3 - x3 * y2;
    c3 = x3 * y1 - x1 * y3;

    /* vypocet hranove fce (vzdalenost od primky) pro protejsi body */
    s1 = a1 * x3 + b1 * y3 + c1;
    s2 = a2 * x1 + b2 * y1 + c2;
    s3 = a3 * x2 + b3 * y2 + c3;

    if ( !s1 || !s2 || !s3 )
    {
        return;
    }

    /* normalizace, aby vzdalenost od primky byla kladna uvnitr trojuhelniku */
    if( s1 < 0 )
    {
        a1 *= -1;
        b1 *= -1;
        c1 *= -1;
    }
    if( s2 < 0 )
    {
        a2 *= -1;
        b2 *= -1;
        c2 *= -1;
    }
    if( s3 < 0 )
    {
        a3 *= -1;
        b3 *= -1;
        c3 *= -1;
    }

    /* koeficienty pro barycentricke souradnice */
    alpha = 1.0 / ABS(s2);
    beta = 1.0 / ABS(s3);
    gamma = 1.0 / ABS(s1);

    /* vyplnovani... */
    for( y = miny; y <= maxy; ++y )
    {
        /* inicilizace hranove fce v bode (minx, y) */
        e1 = a1 * minx + b1 * y + c1;
        e2 = a2 * minx + b2 * y + c2;
        e3 = a3 * minx + b3 * y + c3;

        for( x = minx; x <= maxx; ++x )
        {
            if( e1 >= 0 && e2 >= 0 && e3 >= 0 )
            {
                /* interpolace pomoci barycentrickych souradnic
                   e1, e2, e3 je aktualni vzdalenost bodu (x, y) od primek */
                w1 = alpha * e2;
                w2 = beta * e3;
                w3 = gamma * e1;

                denom = w1 * oh1 + w2 * oh2 + w3 * oh3;
                u = (w1 * ux1 + w2 * ux2 + w3 * ux3) / denom;
                v = (w1 * uy1 + w2 * uy2 + w3 * uy3) / denom;
                z = w1 * v1->z + w2 * v2->z + w3 * v3->z;

                /* interpolace barvy */
                color = studrenTextureValue((S_StudentRenderer*)pRenderer, u, v);
                color.red = ROUND2BYTE((color.red * (w1 * col1.red + w2 * col2.red + w3 * col3.red)) / 255.0);
                color.green = ROUND2BYTE((color.green * (w1 * col1.green + w2 * col2.green + w3 * col3.green)) / 255.0);
                color.blue = ROUND2BYTE((color.blue * (w1 * col1.blue + w2 * col2.blue + w3 * col3.blue)) / 255.0);

                /* vykresleni bodu */
                if( z < DEPTH(pRenderer, x, y) )
                {
                    PIXEL(pRenderer, x, y) = color;
                    DEPTH(pRenderer, x, y) = z;
                }
            }

            /* hranova fce o pixel vedle */
            e1 += a1;
            e2 += a2;
            e3 += a3;
        }
    }
}

static inline S_Coords studrenInterpolate(const ptr_S_Coords first, const ptr_S_Coords second, float delta)
{
    S_Coords newCoord;

    /*
    float negDelta = 1 - delta;
    newCoord.x = negDelta * first->x + delta * second->x;
    newCoord.y = negDelta * first->y + delta * second->y;
    newCoord.z = negDelta * first->z + delta * second->z;
     */
    newCoord.x = first->x + (second->x - first->x) * delta;
    newCoord.y = first->y + (second->y - first->y) * delta;
    newCoord.z = first->z + (second->z - first->z) * delta;

    return newCoord;
}

/******************************************************************************
 * Vykresli i-ty trojuhelnik n-teho klicoveho snimku modelu
 * pomoci nove fce studrenDrawTriangle()
 * Pred vykreslenim aplikuje na vrcholy a normaly trojuhelniku
 * aktualne nastavene transformacni matice!
 * Upravte tak, aby se model vykreslil interpolovane dle parametru n
 * (cela cast n udava klicovy snimek, desetinna cast n parametr interpolace
 * mezi snimkem n a n + 1)
 * i - index trojuhelniku
 * n - index klicoveho snimku (float pro pozdejsi interpolaci mezi snimky)
 */

void studrenProjectTriangle(S_Renderer *pRenderer, S_Model *pModel, int i, float n)
{
    S_Triangle *triangle;
    int frameNum;
    int CF_VertexOffset, CF_NormalOffset;
    int CF_i0, CF_i1, CF_i2;
    ptr_S_Coords CF_v0, CF_v1, CF_v2, CF_norm;
    ptr_S_Coords CF_n0, CF_n1, CF_n2;
    ptr_S_Coords NF_v0, NF_v1, NF_v2, NF_norm;
    ptr_S_Coords NF_n0, NF_n1, NF_n2;

    IZG_ASSERT(pRenderer && pModel && i >= 0 && i < trivecSize(pModel->triangles) && n >= 0 );

    triangle = trivecGetPtr(pModel->triangles, i);

    frameNum = ((int) n);
    CF_VertexOffset = (frameNum) * pModel->verticesPerFrame;
    CF_NormalOffset = (frameNum) * pModel->triangles->size;

    CF_i0 = triangle->v[0] + CF_VertexOffset;
    CF_i1 = triangle->v[1] + CF_VertexOffset;
    CF_i2 = triangle->v[2] + CF_VertexOffset;
    CF_v0 = cvecGetPtr(pModel->vertices, CF_i0);
    CF_v1 = cvecGetPtr(pModel->vertices, CF_i1);
    CF_v2 = cvecGetPtr(pModel->vertices, CF_i2);
    CF_norm = cvecGetPtr(pModel->trinormals, triangle->n + CF_NormalOffset);
    CF_n0 = cvecGetPtr(pModel->normals, CF_i0);
    CF_n1 = cvecGetPtr(pModel->normals, CF_i1);
    CF_n2 = cvecGetPtr(pModel->normals, CF_i2);

    S_Coords new_v0, new_v1, new_v2, new_norm;
    S_Coords new_n0, new_n1, new_n2;

    float val = fmodf(n, 1.0f);

    if (frameNum != pModel->frames - 1)
    {
        NF_v0 = CF_v0 + pModel->verticesPerFrame;
        NF_v1 = CF_v1 + pModel->verticesPerFrame;
        NF_v2 = CF_v2 + pModel->verticesPerFrame;
        NF_norm = CF_norm + pModel->triangles->size;
        NF_n0 = CF_n0 + pModel->verticesPerFrame;
        NF_n1 = CF_n1 + pModel->verticesPerFrame;
        NF_n2 = CF_n2 + pModel->verticesPerFrame;
    }
    else
    {
        NF_v0 = cvecGetPtr(pModel->vertices, triangle->v[0]);
        NF_v1 = cvecGetPtr(pModel->vertices, triangle->v[1]);
        NF_v2 = cvecGetPtr(pModel->vertices, triangle->v[2]);
        NF_norm = cvecGetPtr(pModel->trinormals, triangle->n);
        NF_n0 = cvecGetPtr(pModel->normals, triangle->v[0]);
        NF_n1 = cvecGetPtr(pModel->normals, triangle->v[1]);
        NF_n2 = cvecGetPtr(pModel->normals, triangle->v[2]);
    }

    new_v0 = studrenInterpolate(CF_v0, NF_v0, val);
    new_v1 = studrenInterpolate(CF_v1, NF_v1, val);
    new_v2 = studrenInterpolate(CF_v2, NF_v2, val);
    new_norm = studrenInterpolate(CF_norm, NF_norm, val);
    new_n0 = studrenInterpolate(CF_n0, NF_n0, val);
    new_n1 = studrenInterpolate(CF_n1, NF_n1, val);
    new_n2 = studrenInterpolate(CF_n2, NF_n2, val);

    S_Coords aa, bb, cc; /* souradnice vrcholu po transformaci */
    /* transformace vrcholu matici model */
    trTransformVertex(&aa, &new_v0);
    trTransformVertex(&bb, &new_v1);
    trTransformVertex(&cc, &new_v2);

    int u1, v1, u2, v2, u3, v3; /* souradnice vrcholu po projekci do roviny obrazovky */
    double h1, h2, h3;
    /* promitneme vrcholy trojuhelniku na obrazovku */
    h1 = trProjectVertex(&u1, &v1, &aa);
    h2 = trProjectVertex(&u2, &v2, &bb);
    h3 = trProjectVertex(&u3, &v3, &cc);

    S_Coords naa, nbb, ncc;          /* normaly ve vrcholech po transformaci */
    /* pro osvetlovaci model transformujeme take normaly ve vrcholech */
    trTransformVector(&naa, &new_n0);
    trTransformVector(&nbb, &new_n1);
    trTransformVector(&ncc, &new_n2);

    /* normalizace normal */
    coordsNormalize(&naa);
    coordsNormalize(&nbb);
    coordsNormalize(&ncc);

    S_Coords nn;                     /* normala trojuhelniku po transformaci */
    /* transformace normaly trojuhelniku matici model */
    trTransformVector(&nn, &new_norm);

    /* normalizace normaly */
    coordsNormalize(&nn);

    /* je troj. privraceny ke kamere, tudiz viditelny? */
    if( !renCalcVisibility(pRenderer, &aa, &nn) )
    {
        /* odvracene troj. vubec nekreslime */
        return;
    }


    /* rasterizace trojuhelniku */
    studrenDrawTriangle(pRenderer,
                    &aa, &bb, &cc,
                    &naa, &nbb, &ncc,
                    u1, v1, h1,
                    u2, v2, h2,
                    u3, v3, h3,
                    &triangle->t[0], &triangle->t[1], &triangle->t[2]
    );
}

void studrenProjectTriangleSimple(S_Renderer *pRenderer, S_Model *pModel, int i)
{
    S_Triangle *triangle;
    int CF_i0, CF_i1, CF_i2;
    ptr_S_Coords CF_v0, CF_v1, CF_v2, CF_norm;
    ptr_S_Coords CF_n0, CF_n1, CF_n2;

    IZG_ASSERT(pRenderer && pModel && i >= 0 && i < trivecSize(pModel->triangles));

    triangle = trivecGetPtr(pModel->triangles, i);

    CF_i0 = triangle->v[0];
    CF_i1 = triangle->v[1];
    CF_i2 = triangle->v[2];
    CF_v0 = cvecGetPtr(pModel->vertices, CF_i0);
    CF_v1 = cvecGetPtr(pModel->vertices, CF_i1);
    CF_v2 = cvecGetPtr(pModel->vertices, CF_i2);
    CF_norm = cvecGetPtr(pModel->trinormals, triangle->n);
    CF_n0 = cvecGetPtr(pModel->normals, CF_i0);
    CF_n1 = cvecGetPtr(pModel->normals, CF_i1);
    CF_n2 = cvecGetPtr(pModel->normals, CF_i2);

    S_Coords aa, bb, cc; /* souradnice vrcholu po transformaci */
    /* transformace vrcholu matici model */
    trTransformVertex(&aa, CF_v0);
    trTransformVertex(&bb, CF_v1);
    trTransformVertex(&cc, CF_v2);

    int u1, v1, u2, v2, u3, v3; /* souradnice vrcholu po projekci do roviny obrazovky */
    double h1, h2, h3;
    /* promitneme vrcholy trojuhelniku na obrazovku */
    h1 = trProjectVertex(&u1, &v1, &aa);
    h2 = trProjectVertex(&u2, &v2, &bb);
    h3 = trProjectVertex(&u3, &v3, &cc);

    S_Coords naa, nbb, ncc;          /* normaly ve vrcholech po transformaci */
    /* pro osvetlovaci model transformujeme take normaly ve vrcholech */
    trTransformVector(&naa, CF_n0);
    trTransformVector(&nbb, CF_n1);
    trTransformVector(&ncc, CF_n2);

    /* normalizace normal */
    coordsNormalize(&naa);
    coordsNormalize(&nbb);
    coordsNormalize(&ncc);

    S_Coords nn;                     /* normala trojuhelniku po transformaci */
    /* transformace normaly trojuhelniku matici model */
    trTransformVector(&nn, CF_norm);

    /* normalizace normaly */
    coordsNormalize(&nn);

    /* je troj. privraceny ke kamere, tudiz viditelny? */
    if( !renCalcVisibility(pRenderer, &aa, &nn) )
    {
        /* odvracene troj. vubec nekreslime */
        return;
    }


    /* rasterizace trojuhelniku */
    studrenDrawTriangle(pRenderer,
                        &aa, &bb, &cc,
                        &naa, &nbb, &ncc,
                        u1, v1, h1,
                        u2, v2, h2,
                        u3, v3, h3,
                        &triangle->t[0], &triangle->t[1], &triangle->t[2]
    );
}

/******************************************************************************
* Vraci hodnotu v aktualne nastavene texture na zadanych
* texturovacich souradnicich u, v
* Pro urceni hodnoty pouziva bilinearni interpolaci
* Pro otestovani vraci ve vychozim stavu barevnou sachovnici dle uv souradnic
* u, v - texturovaci souradnice v intervalu 0..1, ktery odpovida sirce/vysce textury
*/

S_RGBA studrenTextureValue( S_StudentRenderer * pRenderer, double u, double v)
{
    /* ??? */
    /*
    unsigned char c = ROUND2BYTE( ( ( fmod( u * 10.0, 1.0 ) > 0.5 ) ^ ( fmod( v * 10.0, 1.0 ) < 0.5 ) ) * 255 );
    return makeColor( c, 255 - c, 0 );
     */

    double x, y, c1, c2, c3, c4;
    int x1, x2, y1, y2, startIndex;
    S_RGBA color, col11, col21, col12, col22;

    const int width = pRenderer->texture_width;
    const int height = pRenderer->texture_height;

    if (width == 1 && width == height)
        return pRenderer->texture_data[0];

    x = u * (width - 1);
    y = v * (height - 1);

    x1 = (int)floor(x);
    x2 = (int)ceil(x);
    y1 = (int)floor(y);
    y2 = (int)ceil(y);

    startIndex = y1 + x2 * height;
    col11 = pRenderer->texture_data[startIndex];
    col21 = pRenderer->texture_data[startIndex + height];
    col12 = pRenderer->texture_data[startIndex + 1];
    col22 = pRenderer->texture_data[startIndex + height + 1];

    c1 = (x2 - x) * (y2 - y);
    c2 = (x - x1) * (y2 - y);
    c3 = (x2 - x) * (y - y1);
    c4 = (x - x1) * (y - y1);

    color.red = ROUND2BYTE(col11.red * c1 + col21.red * c2 + col12.red * c3 + col22.red * c4);
    color.green = ROUND2BYTE(col11.green * c1 + col21.green * c2 + col12.green * c3 + col22.green * c4);
    color.blue = ROUND2BYTE(col11.blue * c1 + col21.blue * c2 + col12.blue * c3 + col22.blue * c4);
    color.alpha = ROUND2BYTE(col11.alpha * c1 + col21.alpha * c2 + col12.alpha * c3 + col22.alpha * c4);

    return color;
}

/******************************************************************************
 ******************************************************************************
 * Funkce pro vyrenderovani sceny, tj. vykresleni modelu
 * Upravte tak, aby se model vykreslil animovane
 * (volani renderModel s aktualizovanym parametrem n)
 */

void renderStudentScene(S_Renderer *pRenderer, S_Model *pModel)
{
    /* test existence frame bufferu a modelu */
    IZG_ASSERT(pModel && pRenderer);

    /* nastavit projekcni matici */
    trProjectionPerspective(pRenderer->camera_dist, pRenderer->frame_w, pRenderer->frame_h);

    /* vycistit model matici */
    trLoadIdentity();

    /* nejprve nastavime posuv cele sceny od/ke kamere */
    trTranslate(0.0, 0.0, pRenderer->scene_move_z);

    /* nejprve nastavime posuv cele sceny v rovine XY */
    trTranslate(pRenderer->scene_move_x, pRenderer->scene_move_y, 0.0);

    /* natoceni cele sceny - jen ve dvou smerech - mys je jen 2D... :( */
    trRotateX(pRenderer->scene_rot_x);
    trRotateY(pRenderer->scene_rot_y);

    /* nastavime material */
    renMatAmbient(pRenderer, &MAT_WHITE_AMBIENT);
    renMatDiffuse(pRenderer, &MAT_WHITE_DIFFUSE);
    renMatSpecular(pRenderer, &MAT_WHITE_SPECULAR);

    /* a vykreslime nas model (ve vychozim stavu kreslime pouze snimek 0) */
    if (pModel->frames > 1)
        for(int iii = 0; iii < trivecSize(pModel->triangles); ++iii)
            studrenProjectTriangle(pRenderer, pModel, iii, fmodf(numTicks / FRAME_LENGTH, pModel->frames));
    else
        for(int iii = 0; iii < trivecSize(pModel->triangles); ++iii)
            studrenProjectTriangleSimple(pRenderer, pModel, iii);
    //renderModel(pRenderer, pModel, fmodf(numTicks / FRAME_LENGTH, pModel->frames));
}

/* Callback funkce volana pri tiknuti casovace
 * ticks - pocet milisekund od inicializace */
void onTimer( int ticks )
{
    /* uprava parametru pouzivaneho pro vyber klicoveho snimku
     * a pro interpolaci mezi snimky */
    /* ??? */
    numTicks = ticks;
}

/*****************************************************************************
 *****************************************************************************/
